FROM nodered/node-red-docker:v8

VOLUME ["/data"]

USER root

RUN echo "deb http://ftp.debian.org/debian jessie-backports main" | tee /etc/apt/sources.list.d/jessie-backports.list \
	&& apt update \
	&& apt install -t jessie-backports -y ca-certificates-java openjdk-8-jre-headless

RUN curl https://downloads.arduino.cc/arduino-1.8.5-linux64.tar.xz | tar xJC /

# Upgrade to latest npm due to https://github.com/npm/npm/issues/17379
RUN npm install -g npm@latest

COPY git-credentials /root/.git-credentials

RUN git config --global credential.helper store \
&& npm install \
	node-red-node-serialport \
	git+https://gitlab.com/enact/node-red-contrib-thingml-compiler \
	git+https://gitlab.com/enact/node-red-contrib-thingml-preparedeploy-docker \
	git+https://gitlab.com/enact/node-red-contrib-docker-compose \
	git+https://gitlab.com/enact/node-red-contrib-arduino

#	&& chmod 755 node_modules/node-red-contrib-thingml/lib

ENV PATH="${PATH}:/arduino-1.8.5"

#USER node-red
