# docker-node-red-thingml

 * **node-red-contrib-thingml**: x86\_64 Linux image
 * **node-red-contrib-thingml-rpi**: ARM Raspberry Pi image

## Build

In the `git-credentials` file, replace `username` and `password` with your actual Gitlab.com username and password. It is required for `npm` to fetch the modules from the private Enact project repositories.

On a PC:
```
docker-compose build node-red-contrib-thingml
```

On a Raspberry Pi:
```
docker-compose build node-red-contrib-thingml-rpi
```

## Run

On a PC:
```
docker-compose up node-red-contrib-thingml
```

On a Raspberry Pi:
```
docker-compose up node-red-contrib-thingml-rpi
```
